const ws = new WebSocket('ws://localhost:8080');

const chatField = document.querySelector('.chat-field');

ws.addEventListener('open', () => {
  const myNameHeading = document.createElement('h2');
  chatField.appendChild(myNameHeading);
  myNameHeading.classList = 'chat-field-heading hidden';

  const name = prompt(
    `Enter your name or leave field empty to chat as Anonymous`
  );

  if (name) {
    myNameHeading.classList.remove('hidden');
    myNameHeading.textContent = `${name}'s chat`;
  } else {
    myNameHeading.classList.add('hidden');
  }

  const messageBox = document.querySelector('.messageBox');

  const formBtn = document.querySelector('.messageBtn');

  formBtn.addEventListener('click', () => {
    const message = messageBox.value;
    const time = new Date().toLocaleString();

    const myMessageContainer = document.createElement('article');

    const myMessageOwner = document.createElement('h5');
    const myMessage = document.createElement('p');

    chatField.appendChild(myMessageContainer);
    myMessageContainer.appendChild(myMessageOwner);
    myMessageContainer.appendChild(myMessage);

    myMessageContainer.classList.add('chat-field-message-container-right');
    myMessageOwner.classList.add('chat-field-message-bearer');
    myMessage.classList.add('chat-field-message');

    myMessageOwner.textContent = `By You @ ${time}`;
    myMessage.textContent = `${message}`;

    ws.send(
      JSON.stringify({
        name,
        message,
        time
      })
    );
  });
});

ws.addEventListener('message', (e) => {
  const data = JSON.parse(e.data);

  const { name, message, time } = data;

  const newMessageContainer = document.createElement('article');

  const newMessageOwner = document.createElement('h5');
  const newMessage = document.createElement('p');

  chatField.appendChild(newMessageContainer);
  newMessageContainer.appendChild(newMessageOwner);
  newMessageContainer.appendChild(newMessage);

  newMessageContainer.classList.add('chat-field-message-container');
  newMessageOwner.classList.add('chat-field-message-bearer');
  newMessage.classList.add('chat-field-message');

  newMessageOwner.textContent = `By ${name || 'Anonymous'} @ ${time}`;
  newMessage.textContent = `${message}`;
});
