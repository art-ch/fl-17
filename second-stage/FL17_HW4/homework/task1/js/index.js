const loading = document.querySelector('.loading');
const ul = document.querySelector('.users');

const oneSecond = 1000;

const inputPhoneRegex = new RegExp(
  /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]\d{4}$/g
);
const outputPhoneRegex = new RegExp(
  /(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]\d{4}/g
);

const showLoading = () => {
  loading.classList.remove('hidden');
};
const hideLoading = () => {
  loading.classList.add('hidden');
};

const defaultView = (div, name, phone, email, companyName, website) => {
  div.innerHTML = `
                <h3>${name}</h3>
                <p>Phone Number: ${phone}</p>
                <p>Email: ${email}</p>
                <p>Company: ${companyName}</p>
                <p>Website: ${website}</p>
            `;
};

const getUsers = async () => {
  showLoading();
  const data = await fetch('https://jsonplaceholder.typicode.com/users');
  const users = await data.json();

  return users;
};

const usersList = async () => {
  const users = await getUsers();

  hideLoading();
  return users.forEach((user) => {
    const {
      id,
      name,
      username,
      phone,
      email,
      website,
      company: { name: companyName },
      address: {
        city,
        street,
        suite,
        zipcode,
        geo: { lat, lng }
      }
    } = user;

    const newLi = document.createElement('li');
    const div = document.createElement('div');
    const buttonDiv = document.createElement('div');
    const editBtn = document.createElement('button');
    const cancelBtn = document.createElement('button');
    const deleteBtn = document.createElement('button');

    ul.appendChild(newLi);
    newLi.appendChild(div);
    newLi.appendChild(buttonDiv);
    buttonDiv.appendChild(editBtn);
    buttonDiv.appendChild(cancelBtn);
    buttonDiv.appendChild(deleteBtn);
    buttonDiv.classList.add('button-container');
    editBtn.classList.add('edit-btn');
    cancelBtn.classList.add('hidden');
    deleteBtn.classList.add('delete-btn');

    defaultView(
      div,
      name,
      phone.match(outputPhoneRegex),
      email,
      companyName,
      website
    );

    editBtn.textContent = 'Edit';
    cancelBtn.textContent = 'Cancel';
    deleteBtn.textContent = 'Delete';

    editBtn.addEventListener('click', () => {
      cancelBtn.classList.remove('hidden');
      div.innerHTML = `
      <form>
      <h3>Edit Info About ${name}</h3>
    <div class="input-container">
    <label>Username</label>
    <input type="text" name="username" placeholder="${username}"></input>
    <label>Name</label>
    <input type="text" name="name" placeholder="${name}"></input>
    <label>Phone Number</label>
    <input type="phone" name="phone" placeholder="${phone.match(
      /(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]\d{4}/g
    )}"></input>
    <label>Email</label>
    <input type="email" name="email" placeholder="${email}"></input>
    <label>Company</label>
    <input type="text" name="company" placeholder="${companyName}"></input>
    <label>Website</label>
    <input type="text" name="website" placeholder="${website}"></input>
    <label>Address</label>
    <input type="text" name="street" placeholder="${`${suite}, ${street} Str.`}"></input>
    <input type="text" name="city" placeholder="${`${city}, ${zipcode}`}"></input>
    <input type="text" name="country" placeholder="${`${lat}, ${lng}`}"></input>
    </div>
    <div class="form-btn-container">
      <button type='submit' class='form-btn'>Submit</button>
    </div>
    </form>
    `;

      const form = document.querySelector('form');

      form.addEventListener('submit', (e) => {
        e.preventDefault();
        showLoading();

        setTimeout(() => {
          cancelBtn.classList.add('hidden');

          const newUsername = form.username.value;
          const newName = form.name.value;
          const newPhoneNumber = form.phone.value;
          const newEmail = form.email.value;
          const newCompanyName = form.company.value;
          const newWebsite = form.website.value;
          const newStreet = form.street.value;
          const newCity = form.city.value;
          const newCountry = form.country.value;

          const editedUser = {
            id,
            newUsername,
            newName,
            newPhoneNumber,
            newEmail,
            newCompanyName,
            newWebsite,
            newStreet,
            newCity,
            newCountry
          };

          fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json'
            },
            body: JSON.stringify(editedUser)
          });
          hideLoading();
          defaultView(
            div,
            newName || name,
            newPhoneNumber.match(inputPhoneRegex) ||
              phone.match(outputPhoneRegex),
            newEmail || email,
            newCompanyName || companyName,
            newWebsite || website
          );
        }, oneSecond);
      });
    });

    cancelBtn.addEventListener('click', () => {
      cancelBtn.classList.add('hidden');
      defaultView(
        div,
        name,
        phone.match(outputPhoneRegex),
        email,
        companyName,
        website
      );
      div.innerHTML = `
                <h3>${name}</h3>
                <p>Phone Number: ${phone.match(
                  /(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]\d{4}/g
                )}</p>
                <p>Email: ${email}</p>
                <p>Company: ${companyName}</p>
                <p>Website: ${website}</p>
            `;
    });

    deleteBtn.addEventListener('click', (e) => {
      showLoading();
      setTimeout(() => {
        const userBoundForDeletion = { id };
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
          },
          body: JSON.stringify(userBoundForDeletion)
        });
        e.target.parentElement.parentElement.style.display = 'none';
        hideLoading();
      }, oneSecond);
    });
  });
};
usersList();
