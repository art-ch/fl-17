class Magazine {
  constructor() {
    this.staff = [];
    this.articles = [];
    this.followers = [];

    this.state = 'ReadyForPushNotification';
    this.states = {
      ReadyForPushNotification(name) {
        return {
          publish: `Hello ${name}. You can't publish. We are creating publications now.`,
          approve: `Hello ${name}. You can't approve. We don't have enough of publications.`
        };
      },
      ReadyForApprove(name) {
        return {
          publish: `Hello ${name}. You can't publish. We don't have a manager's approval.`,
          approve: `Hello ${name}. You've approved the changes`
        };
      },
      ReadyForPublish(name) {
        return {
          publish: `Hello ${name}. You've recently published publications.`,
          approve: `Hello ${name}. Publications have been already approved by you.`
        };
      },
      PublishInProgress(name) {
        return {
          publish: `Hello ${name}. While we are publishing we can't do any actions.`,
          approve: `Hello ${name}. While we are publishing we can't do any actions.`
        };
      }
    };
  }

  addEmployee(newEmployee) {
    this.staff.push(newEmployee);
  }

  changeState(newState) {
    this.state = newState;
  }

  subscribe(follower) {
    this.followers.push(follower);
  }
  unsubscribe(followerToRemove) {
    this.followers = this.followers.filter(
      (follower) => follower.name !== followerToRemove.name
    );
  }
  sendArticles() {
    this.followers.forEach((follower) => {
      let data;
      if (!follower.topic) {
        data = this.articles.map((article) => article.text);
      }
      if (follower.topic) {
        data = this.articles
          .filter((article) => article.topic === follower.topic)
          .map((article) => article.text);
      }
      follower.sendData(data);
    });
  }
}

class MagazineEmployee {
  constructor(name, occupation, magazine) {
    this.name = name;
    this.occupation = occupation;
    this.magazine = magazine;
    this.magazine.addEmployee({ name, occupation });
  }

  addArticle(text) {
    const FIVE = 5;
    if (this.occupation !== 'manager') {
      this.magazine.articles.push({ topic: this.occupation, text });
      if (this.magazine.articles.length >= FIVE) {
        this.magazine.changeState('ReadyForApprove');
      }

      return text;
    } else {
      console.log(`C'mon ${this.name}. You don't have to do that`);
    }
  }

  getResponse() {
    return this.magazine.states[this.magazine.state](this.name);
  }
  approve() {
    if (this.occupation === 'manager') {
      console.log(this.getResponse().approve);

      if (this.magazine.state === 'ReadyForApprove') {
        this.magazine.changeState('ReadyForPublish');
      }
    } else {
      console.log(`You do not have permissions to do it`);
    }
  }
  publish() {
    const ONE_MINUTE = 60000;

    console.log(this.getResponse().publish);

    if (this.magazine.state === 'ReadyForPublish') {
      this.magazine.changeState('PublishInProgress');

      this.magazine.sendArticles();

      setTimeout(() => {
        this.magazine.changeState('ReadyForPushNotification');
        this.magazine.followers = [];
        this.magazine.articles = [];
        this.magazine.staff = [];
      }, ONE_MINUTE);
    }
  }
}

class Follower {
  constructor(name) {
    this.name = name;

    this.onUpdate = (data) =>
      data.forEach((article) => console.log(`${article} ${this.name}`));
  }
  subscribeTo(magazine, topic) {
    magazine.subscribe({ name: this.name, topic, sendData: this.onUpdate });
  }
  unsubscribeFrom(magazine) {
    magazine.unsubscribe({ name: this.name });
  }
}

const magazine = new Magazine();
const manager = new MagazineEmployee('Andrii', 'manager', magazine);
const sport = new MagazineEmployee('Serhii', 'sport', magazine);
const politics = new MagazineEmployee('Volodymyr', 'politics', magazine);
const general = new MagazineEmployee('Olha', 'general', magazine);

const iryna = new Follower('Iryna');
const maksym = new Follower('Maksym');
const mariya = new Follower('Mariya');

iryna.subscribeTo(magazine, 'sport');
maksym.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'general');

sport.addArticle('something about sport');
politics.addArticle('something about politics');
general.addArticle('some general information');
politics.addArticle('something about politics again');
sport.approve();
manager.approve();
politics.publish();
sport.addArticle('news about sport');
manager.approve();
sport.publish();

manager.approve();
