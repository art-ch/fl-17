import { alertMessage } from './domTraverse';

const twoSeconds = 2000;

export const displayEditingAlert = () => {
  alertMessage.classList.remove('hidden');
  alertMessage.textContent = `Error! You can't 
tweet about that`;
  setTimeout(() => {
    alertMessage.classList.add('hidden');
    alertMessage.textContent = '';
  }, twoSeconds);
};

export const displayLikedAlert = () => {
  alertMessage.classList.remove('hidden');
  setTimeout(() => {
    alertMessage.classList.add('hidden');
    alertMessage.textContent = '';
  }, twoSeconds);
};
