import { addTweetBtn, list, modifyItem, tweetItems } from './domTraverse';
import { likedItemsBtn, backBtn } from './likedPageAccessButtons.js';
import { displayItem } from './dipslayItem';

const cleanUp = () => {
  while (list.firstChild) {
    list.removeChild(list.firstChild);
  }
};

export const homePage = () => {
  modifyItem.classList.add('hidden');
  tweetItems.classList.remove('hidden');

  const items = JSON.parse(localStorage.getItem('items'));

  addTweetBtn.classList.remove('hidden');
  likedItemsBtn.classList.remove('hidden');
  backBtn.classList.add('hidden');

  cleanUp();

  if (items.length > 0) {
    items
      .sort((a, b) => a.id - b.id)
      .forEach(({ id, value, likedState }) => {
        displayItem(id, value, likedState === 'unliked' ? 'like' : 'liked');
      });
  }

  itemsPage();
};

export const itemsPage = () => {
  if (!localStorage.getItem('items')) {
    localStorage.setItem('items', JSON.stringify([]));
  }
  backBtn.classList = 'backBtn hidden';
  if (!addTweetBtn.classList.contains('hidden')) {
    likedItemsBtn.style.marginLeft = '7rem';
  } else {
    likedItemsBtn.style.marginLeft = '0';
  }

  const items = JSON.parse(localStorage.getItem('items'));

  cleanUp();

  if (items.length > 0) {
    items
      .sort((a, b) => a.id - b.id)
      .forEach(({ id, value, likedState }) => {
        displayItem(id, value, likedState === 'unliked' ? 'like' : 'liked');
      });
  }

  if (items.some((item) => item.likedState === 'liked')) {
    likedItemsBtn.classList.remove('hidden');
  } else {
    likedItemsBtn.classList.add('hidden');
  }
};

export const likedItemsPage = () => {
  const items = JSON.parse(localStorage.getItem('items'));
  const likedItems = items.filter((el) => el.likedState === 'liked');

  cleanUp();

  if (likedItems.length > 0) {
    likedItems
      .sort((a, b) => a.id - b.id)
      .forEach(({ id, value, likedState }) => {
        displayItem(id, value, likedState);
      });
  }

  addTweetBtn.classList.add('hidden');
  likedItemsBtn.classList.add('hidden');

  backBtn.classList.remove('hidden');
  backBtn.addEventListener('click', () => {
    history.pushState(null, '', '/index.html');
    homePage();
  });
};
