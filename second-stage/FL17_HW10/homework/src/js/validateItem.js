export const validateItem = (input) => {
  const minlength = 1;
  const maxLength = 140;
  const validatedTweet =
    input && input.length >= minlength && input.length <= maxLength;

  const items = JSON.parse(localStorage.getItem('items'));

  if (!items.some((item) => item.value === input)) {
    return validatedTweet;
  }
};
