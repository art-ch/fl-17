import { navigationButtons } from './domTraverse';

export const likedItemsBtn = document.createElement('button');
navigationButtons.appendChild(likedItemsBtn);
likedItemsBtn.classList = 'goToLikedBtn hidden';
likedItemsBtn.textContent = 'Go to Liked';

export const backBtn = document.createElement('button');
navigationButtons.appendChild(backBtn);
backBtn.classList = 'backBtn hidden';
backBtn.textContent = 'back';
