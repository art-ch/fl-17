export const alertMessage = document.getElementById('alertMessage');

export const tweetItems = document.getElementById('tweetItems');
export const navigationButtons = document.getElementById('navigationButtons');
export const addTweetBtn = document.querySelector('.addTweet');
export const list = document.querySelector('#list');

export const modifyItem = document.getElementById('modifyItem');
export const modifyItemHeader = document.getElementById('modifyItemHeader');
export const modifyItemInput = document.getElementById('modifyItemInput');
export const cancelModification = document.getElementById('cancelModification');
export const saveModifiedItem = document.getElementById('saveModifiedItem');
