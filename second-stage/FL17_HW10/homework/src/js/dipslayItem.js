import { list, alertMessage, modifyItemInput } from './domTraverse.js';
import { likedItemsBtn, backBtn } from './likedPageAccessButtons.js';
import { displayLikedAlert } from './alerts';
import { editItemPage } from './modifyItemPages';

export const displayItem = (id, value, likeState) => {
  const newLi = document.createElement('li');
  const newP = document.createElement('p');
  const underlineDiv = document.createElement('div');
  const newDiv = document.createElement('div');
  const newRemoveBtn = document.createElement('button');
  const newLikeBtn = document.createElement('button');

  list.appendChild(newLi);

  newLi.appendChild(newP);
  newLi.appendChild(underlineDiv);
  newLi.appendChild(newDiv);

  underlineDiv.classList.add('underline');

  newDiv.appendChild(newRemoveBtn);
  newDiv.appendChild(newLikeBtn);

  newLi.setAttribute('data-id', id);
  newLikeBtn.setAttribute('data-liked', 'unliked');
  newP.classList.add('tweetContent');

  newRemoveBtn.classList.add('remove-btn');
  newLikeBtn.classList.add('like-btn');

  newP.textContent = value;
  newRemoveBtn.textContent = 'remove';
  newLikeBtn.textContent = likeState || 'like';
  if (likeState === 'liked') {
    newLikeBtn.classList.add('btn-liked');
  }

  newP.addEventListener('click', (e) => {
    const id = e.target.parentElement.dataset.id;

    modifyItemInput.value = e.target.textContent;

    history.pushState({ id }, null, `#/edit/${id}`);

    editItemPage();
  });

  newRemoveBtn.addEventListener('click', () => {
    const items = JSON.parse(localStorage.getItem('items'));
    const targetItem = items.filter((item) => item.id === id)[0];

    const newItems = items.filter((item) => item.id !== targetItem.id);

    localStorage.setItem('items', JSON.stringify(newItems));
    newLi.remove();
  });

  newLikeBtn.addEventListener('click', (e) => {
    const id = e.target.parentElement.parentElement.dataset.id;

    const items = JSON.parse(localStorage.getItem('items'));
    const targetItem = items.filter((item) => item.id === id)[0];

    if (e.target.textContent === 'like') {
      e.target.dataset.liked = 'liked';
      targetItem.likedState = 'liked';
      e.target.textContent = 'liked';
      e.target.classList.add('btn-liked');
      if (backBtn.classList.contains('hidden')) {
        likedItemsBtn.classList.remove('hidden');
      }
      alertMessage.textContent = `Hooray! You liked tweet with id ${id}!`;
      displayLikedAlert();
    } else {
      e.target.dataset.liked = 'unliked';
      targetItem.likedState = 'unliked';
      e.target.textContent = 'like';
      e.target.classList.remove('btn-liked');
      if (!backBtn.classList.contains('hidden')) {
        e.target.parentElement.parentElement.remove();
      }
      alertMessage.textContent = `Sorry you no longer like tweet with id ${id}`;
      displayLikedAlert();
    }

    if (items.every((item) => item.likedState === 'unliked')) {
      likedItemsBtn.classList.add('hidden');
    }

    localStorage.setItem('items', JSON.stringify(items));
  });
};
