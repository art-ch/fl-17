import { validateItem } from './validateItem.js';
import {
  addTweetBtn,
  modifyItemInput,
  cancelModification,
  saveModifiedItem
} from './domTraverse';
import { likedItemsBtn } from './likedPageAccessButtons.js';
import { addItemPage, editItemPage } from './modifyItemPages.js';
import { displayEditingAlert } from './alerts.js';
import { homePage, itemsPage, likedItemsPage } from './mainPages.js';
import { displayItem } from './dipslayItem.js';

const app = () => {
  window.addEventListener('popstate', () => {
    if (!location.hash) {
      history.replaceState(null, '', '/index.html');
      homePage();
    } else {
      if (location.hash === '#/add') {
        addItemPage();
      }
      if (location.hash === '#/liked') {
        homePage();
        likedItemsPage();
      }
      if (location.hash.includes('#/edit/')) {
        const id = location.hash.match(/[0-9]{1,}/)[0];
        const items = JSON.parse(localStorage.getItem('items'));
        const targetItem = items.filter((item) => item.id === id)[0];

        modifyItemInput.value = targetItem.value;

        history.replaceState({ id }, null, `#/edit/${id}`);
        editItemPage();
      }
    }
  });

  document.addEventListener('DOMContentLoaded', () => {
    itemsPage();
  });

  addTweetBtn.addEventListener('click', () => {
    history.pushState(null, '', '#/add');
    addItemPage();
  });

  likedItemsBtn.addEventListener('click', () => {
    history.pushState(null, '', '#/liked');
    likedItemsPage();
  });

  saveModifiedItem.addEventListener('click', () => {
    if (history.state) {
      if (validateItem(modifyItemInput.value)) {
        const items = JSON.parse(localStorage.getItem('items'));
        const targetItem = items.filter(
          (item) => item.id === history.state.id
        )[0];

        targetItem.value = modifyItemInput.value;
        localStorage.setItem('items', JSON.stringify(items));

        history.pushState(null, '', '/index.html');
        homePage();
      } else {
        displayEditingAlert();
      }
    } else {
      if (validateItem(modifyItemInput.value)) {
        displayItem(new Date().getTime().toString(), modifyItemInput.value);

        const ul = document.querySelector('ul');
        const likeBtn = document.querySelector('.like-btn');

        const items = JSON.parse(localStorage.getItem('items'));

        const newItem = {
          id: ul.lastChild.dataset.id,
          value: modifyItemInput.value,
          likedState: likeBtn.dataset.liked
        };

        items.push(newItem);
        localStorage.setItem('items', JSON.stringify(items));

        history.pushState(null, '', '/index.html');
        homePage();
      } else {
        displayEditingAlert();
      }
    }
  });

  cancelModification.addEventListener('click', () => {
    history.replaceState(null, '', '/index.html');
    homePage();
  });
};

export default app;
