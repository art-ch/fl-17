import {
  tweetItems,
  modifyItem,
  modifyItemHeader,
  modifyItemInput
} from './domTraverse';

export const addItemPage = () => {
  tweetItems.classList.add('hidden');
  modifyItem.removeAttribute('class');
  modifyItemHeader.textContent = 'Add Tweet';
  modifyItemInput.value = '';
};

export const editItemPage = () => {
  tweetItems.classList.add('hidden');
  modifyItem.removeAttribute('class');
  modifyItemHeader.textContent = 'Edit Tweet';
};
