// write your code here

const getMaxEvenElement = (arr) => {
  const TWO = 2;
  const evenArray = arr.filter((num) => parseInt(num) % TWO === 0);
  const result = Math.max(...evenArray);

  return result;
};

let a = 3;
let b = 5;

[a, b] = [b, a];

console.log(a);
console.log(b);

const getValue = (value) => value ?? '-';

const getObjFromArray = (arrayOfArrays) => {
  const map = new Map(arrayOfArrays);
  const obj = Object.fromEntries(map);

  return obj;
};

const addUniqueId = (obj) => {
  const id = Symbol();
  const result = { ...obj, id };

  return result;
};

const getRegroupedObject = (obj) => {
  const {
    name: firstName,
    details: { id, age, university }
  } = obj;

  const newObj = { university, user: { age, firstName, id } };

  return newObj;
};

const getArrayWithUniqueElements = (arr) => [...new Set(arr)];

const hideNumber = (phoneNumber) => {
  const NEGATED_AMOUNT_OF_VISIBLE_NUMBERS = -4;
  const last4Digits = phoneNumber.slice(NEGATED_AMOUNT_OF_VISIBLE_NUMBERS);
  const maskedNumber = last4Digits.padStart(phoneNumber.length, '*');

  return maskedNumber;
};

const requires = (value) => {
  throw new Error(`${value} is required`);
};
const add = (a = requires('a'), b = requires('b')) => a + b;

function* generateIterableSequence() {
  yield 'I';
  yield 'love';
  yield 'EPAM';
}
