import '../scss/utils.scss';
import '../scss/global.scss';
import '../scss/rules.scss';
import '../scss/callToAction.scss';
import '../scss/gameSection.scss';
import '../index.html';

import app from './app/app.js';

app();
