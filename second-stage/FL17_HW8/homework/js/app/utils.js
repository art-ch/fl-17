export const getElement = (selection) => {
  const element = document.querySelector(selection);
  if (element) {
    return element;
  }
  throw new Error(
    `Please check "${selection}" selector, no such element exist`
  );
};

export const getRandomValue = () => {
  const TWO = 2;
  const arrayOfValues = ['Rock', 'Paper', 'Scissors'];
  const randomValue =
    arrayOfValues[parseInt(Math.floor(Math.random() * (TWO - 0 + 1) + 0))];

  return randomValue;
};
