import { getElement } from './utils';

export const playBtn = document.querySelectorAll('.play-btn');
export const rockBtn = getElement('.rock-btn');
export const paperBtn = getElement('.paper-btn');
export const scissorsBtn = getElement('.scissors-btn');
export const resetBtn = getElement('.reset-btn');

export const playerValueContainer = getElement('.player-value-container');
export const computerValueContainer = getElement('.computer-value-container');
export const logContainer = getElement('.log-container');
export const finalResult = getElement('.final-result');
export const finalResultSpan = getElement('.final-result-span');
