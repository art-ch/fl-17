import { getRandomValue } from './utils.js';
import { playerValueContainer, computerValueContainer } from './domTraverse';

import Rock from '../../img/Rock.jpg';
import Paper from '../../img/Paper.jpg';
import Scissors from '../../img/Scissors.jpg';

export const handleRound = (playerValue) => {
  const computerValue = getRandomValue();

  const images = { Rock, Paper, Scissors };

  playerValueContainer.innerHTML = `
  <img src="${images[playerValue]}" alt="" />
  <p>${playerValue}</p>
  `;
  computerValueContainer.innerHTML = `
  <img src="${images[computerValue]}" alt="" />
  <p>${computerValue}</p>
  `;

  const currentValues = [playerValue, computerValue];
  const winnerValues = [
    ['Scissors', 'Paper'],
    ['Paper', 'Rock'],
    ['Rock', 'Scissors']
  ];

  const winner = winnerValues.some((values) => {
    return (
      Array.isArray(currentValues) &&
      Array.isArray(values) &&
      currentValues.length === values.length &&
      currentValues.every((v, i) => v === values[i])
    );
  });
  const draw = playerValue === computerValue;
  const loser = !winner && !draw;

  return {
    computerValue,
    // eslint-disable-next-line no-extra-parens
    result: (winner && 'winner') || (draw && 'draw') || (loser && 'loser')
  };
};

export const resetHandler = () => {
  location.reload();
};
