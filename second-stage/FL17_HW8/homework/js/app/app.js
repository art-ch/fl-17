import { handleRound, resetHandler } from './handlers.js';
import {
  playBtn,
  rockBtn,
  paperBtn,
  scissorsBtn,
  resetBtn,
  logContainer,
  finalResult,
  finalResultSpan
} from './domTraverse.js';

const app = () => {
  let roundCounter = 0;
  let roundsLeftCounter = 3;
  let winCounter = 0;
  let loseCounter = 0;
  let message = '';

  const gameLogic = (playerValue) => {
    const roundResult = handleRound(playerValue);

    const { computerValue, result } = roundResult;

    if (result === 'winner') {
      message = `You've WON!`;
      winCounter++;
      roundsLeftCounter--;
    }
    if (result === 'draw') {
      message = `It's a DRAW!`;
    }
    if (result === 'loser') {
      message = `You've LOST!`;
      loseCounter++;
      roundsLeftCounter--;
    }
    if (roundsLeftCounter >= 0) {
      const logEntry = document.createElement('p');
      logEntry.textContent = `Round ${roundCounter}, ${playerValue} vs ${computerValue}, ${message}`;
      logContainer.appendChild(logEntry);
    }
    if (roundsLeftCounter === 0) {
      finalResult.classList.remove('hidden');

      if (winCounter > loseCounter) {
        finalResultSpan.textContent = 'WON!';
        finalResultSpan.style.background = 'green';
      }
      if (winCounter < loseCounter) {
        finalResultSpan.textContent = 'LOST!';
        finalResultSpan.style.background = 'red';
      }

      // eslint-disable-next-line no-extra-parens
      playBtn.forEach((button) => (button.disabled = true));
    }
  };

  rockBtn.addEventListener('click', () => {
    roundCounter++;
    gameLogic('Rock');
  });
  paperBtn.addEventListener('click', () => {
    roundCounter++;
    gameLogic('Paper');
  });
  scissorsBtn.addEventListener('click', () => {
    roundCounter++;
    gameLogic('Scissors');
  });

  resetBtn.addEventListener('click', () => resetHandler());
};

export default app;
