const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = {
  target: ['web', 'es5'],
  mode: 'production',
  entry: {
    main: path.resolve(__dirname, 'js/index.js')
  },
  output: {
    chunkFormat: 'commonjs',
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js',
    clean: true
  },

  module: {
    rules: [
      { test: /\.html$/i, use: ['html-loader'] },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.js$/i,
        exclude: /node-modules/,
        use: ['babel-loader']
      },
      {
        test: /\.jpg$/i,
        exclude: /node-modules/,
        use: ['url-loader']
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'index.html'
    }),
    new MiniCssExtractPlugin()
  ],

  optimization: {
    minimizer: [new OptimizeCssAssetsWebpackPlugin(), new TerserWebpackPlugin()]
  }
};
