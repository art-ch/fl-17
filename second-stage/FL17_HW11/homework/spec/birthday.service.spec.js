const birthdayService = require('../src/birthday.service');

describe('Test birthdayService', () => {
  let originalTimeout;
  beforeAll(() => {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100;
  });

  it('should only accept date object as argument', () => {
    const randomNumber = 42069;

    const wrongValues = [
      '',
      'random-string',
      randomNumber,
      true,
      false,
      null,
      undefined,
      [],
      {},
      [0, 1, randomNumber],
      { firstName: 'Charlie' }
    ];

    wrongValues.forEach((value) =>
      expect(() => birthdayService.howLongToMyBirthday(value)).toThrowError(
        TypeError,
        'Wrong argument!'
      )
    );
  });

  const SEVENTY = 70;
  const ONE_HUNDRED_EIGHTY_ONE = 181;

  const msPerSec = 1000;
  const secsInHour = 3600;
  const hoursInDay = 24;

  const dayInRange = SEVENTY * hoursInDay * secsInHour * msPerSec;
  const dayOutOfRange =
    ONE_HUNDRED_EIGHTY_ONE * hoursInDay * secsInHour * msPerSec;

  const date = new Date().setHours(0, 0, 0, 0);
  const dateInFuture = new Date(date + dayInRange).setHours(0, 0, 0, 0);
  const dateInPast = new Date(date - dayInRange).setHours(0, 0, 0, 0);
  const dateInDistantPast = new Date(date - dayOutOfRange).setHours(0, 0, 0, 0);

  const dateTestParameters = [
    {
      date,
      timeFrame: 'today',
      message: 'Hooray!!! It is today!'
    },
    {
      date: dateInFuture,
      timeFrame: 'that birthday is in the future',
      message: `Soon...Please, wait just 70 day/days`
    },
    {
      date: dateInPast,
      timeFrame: 'that birthday is in the past',
      message: `Oh, you have celebrated it 70 day/s ago, don't you remember?`
    },
    {
      date: dateInDistantPast,
      timeFrame: '"Range limit is 180 days"',
      message: `Range limit is 180 days`
    }
  ];

  dateTestParameters.forEach((data) => {
    const { date, timeFrame, message } = data;

    it(`should return Promise after 100ms and message ${timeFrame}`, async () => {
      const result = await birthdayService.howLongToMyBirthday(new Date(date));

      expect(result).toEqual(message);
    });
  });

  afterAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
});
