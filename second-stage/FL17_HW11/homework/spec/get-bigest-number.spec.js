const getBiggestNumber = require('../src/get-bigest-number');

describe('Test "getBiggestNumber()" function', () => {
  const HUNDRED = 100;
  const TEN_THOUSAND = 10000;

  const minValidLength = 2;
  const maxValidLength = 10;

  const minInvalidLength = 11;

  const validArrayLength = Math.floor(
    Math.random() * (maxValidLength - minValidLength + 1) + minValidLength
  );
  const invalidArrayLength = Math.floor(
    Math.random() * (HUNDRED - minInvalidLength + 1) + minInvalidLength
  );

  const validArray = Array.from({ length: validArrayLength }, () =>
    Math.floor(Math.random() * TEN_THOUSAND)
  );

  it('should accept [2-10] arguments', () => {
    const randomNumber = Math.floor(Math.random() * (TEN_THOUSAND - 0 + 1) + 0);

    expect(() => getBiggestNumber(randomNumber)).toThrowError(
      RangeError,
      'Not enough arguments'
    );

    const longArray = Array.from({ length: invalidArrayLength }, () =>
      Math.floor(Math.random() * TEN_THOUSAND)
    );

    expect(() => getBiggestNumber(...longArray)).toThrowError(
      RangeError,
      'Too many arguments'
    );
  });

  it('should return the biggest value', () => {
    expect(getBiggestNumber(...validArray)).toBe(Math.max(...validArray));
  });

  it('should only accept numbers', () => {
    const maxValidIndex = 9;

    const randomNumber = Math.floor(
      Math.random() * (maxValidIndex - 0 + 1) + 0
    );

    const arrayWithInvalidType = validArray.map((value, index) =>
      index === randomNumber ? value.toString() : value
    );

    expect(() => getBiggestNumber(arrayWithInvalidType)).toThrowError(
      TypeError,
      'Wrong argument type'
    );
  });
});
