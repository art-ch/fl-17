const getBiggestNumber = (...args) => {
  const minValidLength = 2;
  const maxValidLength = 10;

  const numbers = [...args].sort((a, b) => a - b);

  if (numbers.some((value) => typeof value !== 'number')) {
    throw new TypeError('Wrong argument type');
  }
  if (numbers.length < minValidLength) {
    throw new RangeError('Not enough arguments');
  }
  if (numbers.length > maxValidLength) {
    throw new RangeError('Too many arguments');
  }

  return numbers[numbers.length - 1];
};

module.exports = getBiggestNumber;
