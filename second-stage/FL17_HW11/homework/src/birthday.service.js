const birthdayService = {
  howLongToMyBirthday(date) {
    if (date instanceof Date) {
      const eightyMs = 80;

      const msPerSec = 1000;
      const secsInHour = 3600;
      const hoursInDay = 24;

      const todaysDate = new Date().setHours(0, 0, 0, 0);
      const usersDate = date.setHours(0, 0, 0, 0);

      let timeDifference;
      let timeFrame;

      if (usersDate === todaysDate) {
        timeFrame = 'today';
      }
      if (usersDate > todaysDate) {
        timeDifference = usersDate - todaysDate;
        timeFrame = 'future';
      }
      if (usersDate < todaysDate) {
        timeDifference = todaysDate - usersDate;
        timeFrame = 'past';
      }

      return new Promise((resolve) =>
        setTimeout(() => {
          const differenceInDays = Math.round(
            timeDifference / (msPerSec * secsInHour * hoursInDay)
          );

          resolve(this.getMessage({ differenceInDays, timeFrame }));
        }, eightyMs)
      );
    }

    throw new TypeError('Wrong argument!');
  },

  getMessage(time) {
    const { timeFrame } = time;
    if (timeFrame === 'today') {
      console.log(this.congratulateWithBirthday());
      return this.congratulateWithBirthday();
    } else {
      console.log(this.notifyWaitingTime(time));
      return this.notifyWaitingTime(time);
    }
  },

  congratulateWithBirthday() {
    return 'Hooray!!! It is today!';
  },

  notifyWaitingTime(time) {
    const sixMonths = 181;
    const { differenceInDays, timeFrame } = time;

    if (differenceInDays < sixMonths) {
      if (timeFrame === 'future') {
        return `Soon...Please, wait just ${differenceInDays} day/days`;
      }
      if (timeFrame === 'past') {
        return `Oh, you have celebrated it ${differenceInDays} day/s ago, don't you remember?`;
      }
    }
    return 'Range limit is 180 days';
  }
};

module.exports = birthdayService;
