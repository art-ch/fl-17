'use strict';

const TWO = 2;

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */

function Pizza(size, type) {
  // Validation
  // 2 arguments
  if (arguments.length !== TWO) {
    throw new PizzaException(
      `Required two arguments, given: ${arguments.length}`
    );
  }

  // legal sizes and types
  const legalSize = Pizza.allowedSizes.some(
    (currentSize) => currentSize === size
  );
  if (!legalSize) {
    throw new PizzaException(`Invalid size`);
  }
  const legalType = Pizza.allowedTypes.some(
    (currentType) => currentType === type
  );
  if (!legalType) {
    throw new PizzaException(`Invalid type`);
  }

  // Size and Type
  this.size = size;
  this.type = type;

  // Get Pizza Info

  this.getSize = function () {
    return this.size;
  };
  this.getPrice = function () {
    const getPriceForExtras = () => {
      if (this.extraIngredients.length > 0) {
        return this.extraIngredients
          .map((ingredient) => ingredient.price)
          .reduce((a, b) => a + b);
      }
      return 0;
    };

    return this.size.price + this.type.price + getPriceForExtras();
  };
  this.getPizzaInfo = function () {
    return `Size: ${this.size.sizeName}, type: ${
      this.type.typeName
    }, extra ingredients: ${
      this.extraIngredients.length > 0
        ? this.extraIngredients.map((ingredient) => ingredient.name)
        : 'N/A'
    }, price: ${this.getPrice()}UAH.`;
  };

  // Extra Ingredients

  this.extraIngredients = [];

  this.addExtraIngredient = function (ingredient) {
    // Validation
    // one argument
    if (arguments.length !== 1) {
      throw new PizzaException(
        `Required one argument, given: ${arguments.length}`
      );
    }

    // legality of ingredient
    const legalIngredient = Pizza.allowedExtraIngredients.some(
      (currentIngredient) => currentIngredient === ingredient
    );
    if (!legalIngredient) {
      throw new PizzaException(`Invalid ingredient`);
    }

    // duplicate check
    const duplicate = this.extraIngredients.some(
      (currentIngredient) => currentIngredient === ingredient
    );
    if (duplicate) {
      throw new PizzaException(`Duplicate ingredient`);
    }

    return this.extraIngredients.push(ingredient);
  };
  this.getExtraIngredients = function () {
    return this.extraIngredients;
  };
  this.removeExtraIngredient = function (ingredient) {
    // Validation
    // one argument
    if (arguments.length !== 1) {
      throw new PizzaException(
        `Required one argument, given: ${arguments.length}`
      );
    }

    // legality of ingredient
    const legalIngredient = Pizza.allowedExtraIngredients.some(
      (currentIngredient) => currentIngredient === ingredient
    );

    // existence check
    const ingredientExists = this.extraIngredients.some(
      (currentIngredient) => currentIngredient === ingredient
    );

    if (!legalIngredient || !ingredientExists) {
      throw new PizzaException(`Invalid ingredient`);
    }

    let index;
    for (let i = 0; i < this.extraIngredients.length; i++) {
      if (this.extraIngredients[i] === ingredient) {
        index = i;
      }
    }

    return this.extraIngredients.splice(index, 1);
  };
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = { sizeName: 'SMALL', price: 50 };
Pizza.SIZE_M = { sizeName: 'MEDIUM', price: 75 };
Pizza.SIZE_L = { sizeName: 'LARGE', price: 100 };

Pizza.TYPE_VEGGIE = { typeName: 'VEGGIE', price: 50 };
Pizza.TYPE_MARGHERITA = { typeName: 'MARGHERITA', price: 60 };
Pizza.TYPE_PEPPERONI = { typeName: 'PEPPERONI', price: 70 };

Pizza.EXTRA_TOMATOES = { name: 'TOMATOES', price: 5 };
Pizza.EXTRA_CHEESE = { name: 'CHEESE', price: 7 };
Pizza.EXTRA_MEAT = { name: 'MEAT', price: 9 };

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [
  Pizza.TYPE_VEGGIE,
  Pizza.TYPE_MARGHERITA,
  Pizza.TYPE_PEPPERONI
];
Pizza.allowedExtraIngredients = [
  Pizza.EXTRA_TOMATOES,
  Pizza.EXTRA_CHEESE,
  Pizza.EXTRA_MEAT
];

/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */

function PizzaException(log) {
  this.log = log;
}

/* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
