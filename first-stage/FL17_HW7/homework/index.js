// Your code goes here

// task #1

const getAge = (dateObject) => {
  const ageDifMs = Date.now() - dateObject.getTime();
  const ageDate = new Date(ageDifMs);

  const EPOCH_YEAR = 1970;

  return Math.abs(ageDate.getUTCFullYear() - EPOCH_YEAR);
};

// task #2

const getWeekDay = (dateObject) =>
  dateObject.toLocaleString('en-US', { weekday: 'long' });

// task #3

const getAmountDaysToNewYear = (dateObject) => {
  const msPerSec = 1000;
  const secsInHour = 3600;
  const hoursInDay = 24;
  const daysInYear = 365;

  const newYear = new Date(dateObject.getUTCFullYear() + 1, 0, 1);

  const msDif = newYear.getTime() - dateObject.getTime();

  const result = Math.round(msDif / (msPerSec * secsInHour * hoursInDay));

  return result === 0 ? daysInYear : result;
};

// task #4

const getProgrammersDay = (year) => {
  const daysSinceNewYear = 255;

  const result = new Date(year, 0, 1);
  result.setDate(result.getDate() + daysSinceNewYear);

  const dayNum = result.getDate();
  const monthNum = result.getMonth();
  const month = result.toLocaleString('en-US', { month: 'short' });
  const jahr = result.getUTCFullYear();

  return `${dayNum} ${month}, ${jahr} (${getWeekDay(
    new Date(year, monthNum, dayNum)
  )})`;
};

// task #5

const howFarIs = (string) => {
  const firstLetter = string.substring(0, 1).toUpperCase();
  const otherLetters = string.substring(1);
  const targetWeekday = `${firstLetter}${otherLetters}`;

  let todaysDate = new Date();

  if (targetWeekday === getWeekDay(todaysDate)) {
    return `Hey, today is ${targetWeekday} =)`;
  } else {
    const SIX = 6;
    let amountOf = 0;
    while (amountOf < SIX && targetWeekday !== getWeekDay(todaysDate)) {
      todaysDate.setDate(todaysDate.getDate() + 1);
      amountOf += 1;
    }

    return `It's ${amountOf} day(s) left till ${targetWeekday}.`;
  }
};

// task #6

const isValidIdentifier = (string) =>
  /^[a-zA-Z_$][0-9a-zA-Z_$]*$/g.test(string);

// task #7

const capitalize = (string) =>
  string.replace(/\b[a-z]/g, (match) => match.toUpperCase());

// task #8

const isValidAudioFile = (fileName) => {
  const TWO = 2;
  const array = fileName.split('.');

  const extensionTest =
    array[1].toLowerCase() === 'mp3' ||
    array[1].toLowerCase() === 'flac' ||
    array[1].toLowerCase() === 'alac' ||
    array[1].toLowerCase() === 'aac';

  const alphaTest = /[^A-Za-z]/g.test(array[0]);

  return array.length === TWO && extensionTest && !alphaTest;
};

// task #9

const getHexadecimalColors = (string) => {
  const FOUR = 4;
  const SEVEN = 7;
  const hexArray = string.match(/#[A-Fa-f0-9]{3,6}/g);

  const resultArray = hexArray.filter(
    (el) => el.length === FOUR || el.length === SEVEN
  );

  return resultArray;
};

// task #10

const isValidPassword = (string) => {
  console.log(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/.test(string));
};

// task #11

const addThoudandsSeparators = (number) => {
  const numberToFormat =
    typeof number === 'number' ? number.toString() : number;

  const result = numberToFormat.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

  return result;
};

// task #12

const getUrlsFromText = (text) => {
  if (!text) {
    throw new ReferenceError('text is not defined');
  }
  const result = text.match(
    /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/g
  );

  if (!result) {
    return [];
  }

  return result;
};
