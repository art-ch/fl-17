function visitLink(path) {
  let pageOneCounter = localStorage.getItem('pageOneCounter');
  let pageTwoCounter = localStorage.getItem('pageTwoCounter');
  let pageThreeCounter = localStorage.getItem('pageThreeCounter');

  const localStorageHandler = (counter, stringifiedNumber) => {
    if (counter === null) {
      counter = 0;
      localStorage.setItem(`page${stringifiedNumber}Counter`, counter);
    }
    counter = parseInt(localStorage.getItem(`page${stringifiedNumber}Counter`));
    counter += 1;
    localStorage.setItem(`page${stringifiedNumber}Counter`, counter);
  };

  if (path === 'Page1') {
    localStorageHandler(pageOneCounter, 'One');
  }
  if (path === 'Page2') {
    localStorageHandler(pageTwoCounter, 'Two');
  }
  if (path === 'Page3') {
    localStorageHandler(pageThreeCounter, 'Three');
  }
}

function viewResults() {
  const contentDiv = document.querySelector('#content');
  const button = document.querySelector('.btn');

  const resultContainer = document.createElement('ul');
  resultContainer.setAttribute('class', 'result-container');
  contentDiv.appendChild(resultContainer);

  let pageOneCounter = localStorage.getItem('pageOneCounter');
  let pageTwoCounter = localStorage.getItem('pageTwoCounter');
  let pageThreeCounter = localStorage.getItem('pageThreeCounter');

  const pageCounterArray = [pageOneCounter, pageTwoCounter, pageThreeCounter];

  pageCounterArray.forEach((amount, index) => {
    const resultLi = document.createElement('li');
    resultLi.textContent = `You visited Page${index + 1} ${
      amount || 0
    } time(s)`;
    resultContainer.appendChild(resultLi);
  });

  localStorage.clear();
  button.setAttribute('disabled', true);
  button.style.opacity = 1;
}
