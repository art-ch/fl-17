// Your code goes here

// Task #1

const isEquals = (a, b) => a === b;

// Task #2

const isBigger = (a, b) => a > b;

// Task #3

const storeNames = (...args) => [...args];

// Task #4

const getDifference = (a, b) => {
  return isBigger(a, b) ? a - b : b - a;
};

// Task #5

const negativeCount = (arr) => arr.filter((el) => el < 0).length;

// Task #6

const letterCount = (string, letter) => {
  const regex = new RegExp(letter, 'g');
  if (string.match(regex) === null) {
    return 0;
  }
  return string.match(regex).length;
};

// Task #7

const countPoints = (arr) => {
  const THREE = 3;
  const formattedArray = arr.map((str) =>
    str.split(':').map((str) => Number(str))
  );
  const result = formattedArray
    .map((v) => {
      if (v[0] > v[1]) {
        return THREE;
      }
      if (v[0] < v[1]) {
        return 0;
      }
      return 1;
    })
    .reduce((a, b) => a + b);

  return result;
};
