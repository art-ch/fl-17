/* START TASK 1: Your code goes here */
// ----------------------------------- Assignment Section ----------------------------------- //

const container = document.querySelector('.container-cell');

// ---------------------------------------- Utilities --------------------------------------- //

const getAllSiblings = (elem) => {
  let siblings = [];
  let sibling = elem.parentNode.firstChild;

  while (sibling) {
    if (
      sibling.nodeType === 1 &&
      sibling !== elem &&
      !sibling.classList.contains('selected')
    ) {
      siblings.push(sibling);
    }
    sibling = sibling.nextSibling;
  }

  return siblings;
};

// ----------------------------------- Additional Handlers ---------------------------------- //

const handleRow = (e) => {
  const target = e.target;
  const firstAdjacentSibling = e.target.nextElementSibling;
  const secondAdjacentSibling = firstAdjacentSibling.nextElementSibling;

  const siblingArray = [target, firstAdjacentSibling, secondAdjacentSibling];
  const notSelectedCells = siblingArray.filter((el) => {
    if (el.classList.contains('row-cell')) {
      return el;
    }
    if (
      !el.classList.contains('row-cell') &&
      !el.classList.contains('selected')
    ) {
      return el;
    }
    return null;
  });

  return notSelectedCells.forEach((el) => {
    el.classList.add('blue');
    el.classList.add('selected');
  });
};

const handleSingleCell = (e) => {
  if (!e.target.classList.contains('row-cell')) {
    e.target.classList.add('yellow');
  } else {
    handleRow(e);
  }
};

const handleSpecialCell = (e) => {
  e.target.classList.remove('yellow');
  if (!e.target.classList.contains('blue')) {
    e.target.classList.add('green');
  }
  let sibling = e.currentTarget.firstChild;

  let allSiblings = getAllSiblings(sibling);

  return allSiblings.forEach((el) => {
    el.classList.add('green');
  });
};

// ------------------------------------------- Logic --------------------------------------- //

const taskOneHandler = (e) => {
  if (!e.target.classList.contains('container-cell')) {
    e.target.classList.add('selected');
    handleSingleCell(e);
  }
  if (e.target.classList.contains('special-cell')) {
    handleSpecialCell(e);
  }
};

container.addEventListener('click', taskOneHandler);

/* END TASK 1 */

/* START TASK 2: Your code goes here */
// ----------------------------------- Assignment Section ---------------------------------- //

const alertContainer = document.querySelector('.alert-container');
const alertContent = document.querySelector('.alert');
const phoneField = document.querySelector('.phoneField');
const button = document.querySelector('.submit-btn');

// ---------------------------------------- Utilities --------------------------------------- //

const validatePhoneNumber = (number) => {
  const validator = /^\+380[0-9]{9}$/.test(number);
  if (!validator) {
    button.disabled = true;
    alertContainer.classList.remove('hidden');
    alertContainer.classList.remove('alert-success');
    alertContainer.classList.add('alert-danger');
    phoneField.style.border = '1px solid red';
    alertContent.textContent = `Typed number does not follow format +380*********`;
  } else {
    button.disabled = false;
    phoneField.style.border = '1px solid black';
    alertContainer.classList.add('hidden');
    alertContainer.classList.remove('alert-danger');
  }
};

// ------------------------------------------- Logic --------------------------------------- //

const buttonHandler = (e) => {
  e.preventDefault();
  alertContainer.classList.remove('hidden');
  alertContainer.classList.add('alert-success');
  alertContent.textContent = `Data was successfully sent`;
  button.disabled = true;
};

const fieldHandler = () => {
  validatePhoneNumber(phoneField.value);
};

phoneField.addEventListener('input', fieldHandler);
button.addEventListener('click', buttonHandler);

/* END TASK 2 */

/* START TASK 3: Your code goes here */

// ----------------------------------- Assignment Section ----------------------------------- //

const courtContainer = document.querySelector('.court-container');
const ball = document.querySelector('.ball');
const aScoreZone = document.querySelector('.a-score-zone');
const bScoreZone = document.querySelector('.b-score-zone');
const aScore = document.querySelector('.a-score');
const bScore = document.querySelector('.b-score');
const scoreEventAlert = document.querySelector('.score-event-alert');

// ---------------------------------------- Utilities --------------------------------------- //

const moveBall = (e) => {
  const TWO = 2;
  const rect = e.currentTarget.getBoundingClientRect();
  const x = e.clientX - rect.left - ball.offsetWidth / TWO;
  const y = e.clientY - rect.top - ball.offsetHeight / TWO;

  ball.style.top = `${y}px`;
  ball.style.left = `${x}px`;
  ball.style.transition = '0.5s';
};

// ---------------------------------------- Logic --------------------------------------- //

courtContainer.addEventListener('click', moveBall);

aScoreZone.addEventListener('click', () => {
  const threeSeconds = 3000;
  aScore.textContent = (Number(aScore.textContent) + 1).toString();

  scoreEventAlert.textContent = 'Team A score!';
  scoreEventAlert.style.color = 'blue';
  setTimeout(() => {
    scoreEventAlert.textContent = '';
  }, threeSeconds);
});

bScoreZone.addEventListener('click', () => {
  const threeSeconds = 3000;
  bScore.textContent = (Number(bScore.textContent) + 1).toString();

  scoreEventAlert.textContent = 'Team B score!';
  scoreEventAlert.style.color = 'red';
  setTimeout(() => {
    scoreEventAlert.textContent = '';
  }, threeSeconds);
});

/* END TASK 3 */
