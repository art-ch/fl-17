// ------------------------------------- Assigning Area ----------------------------------- //

const root = document.getElementById('root');

const alertMessage = document.getElementById('alertMessage');

const tweetItems = document.getElementById('tweetItems');
const navigationButtons = document.getElementById('navigationButtons');
const addTweetBtn = document.querySelector('.addTweet');
const list = document.querySelector('#list');

const modifyItem = document.getElementById('modifyItem');
const modifyItemHeader = document.getElementById('modifyItemHeader');
const modifyItemInput = document.getElementById('modifyItemInput');
const cancelModification = document.getElementById('cancelModification');
const saveModifiedItem = document.getElementById('saveModifiedItem');

const twoSeconds = 2000;

// -------------------------------------- Static Btns ------------------------------------- //

const likedItemsBtn = document.createElement('button');
navigationButtons.appendChild(likedItemsBtn);
likedItemsBtn.classList = 'goToLikedBtn hidden';
likedItemsBtn.textContent = 'Go to Liked';

const backBtn = document.createElement('button');
navigationButtons.appendChild(backBtn);
backBtn.classList = 'backBtn hidden';
backBtn.textContent = 'back';

// --------------------------------------- Utilities --------------------------------------- //

const validateItem = (input) => {
  const minlength = 1;
  const maxLength = 140;
  const validatedTweet =
    input && input.length >= minlength && input.length <= maxLength;

  const items = JSON.parse(localStorage.getItem('items'));

  if (!items.some((item) => item.value === input)) {
    return validatedTweet;
  }
};

const cleanUp = () => {
  while (list.firstChild) {
    list.removeChild(list.firstChild);
  }
};

// ----------------------------------- Non-page Components --------------------------------- //

const homePage = () => {
  modifyItem.classList.add('hidden');
  tweetItems.classList.remove('hidden');

  const items = JSON.parse(localStorage.getItem('items'));

  addTweetBtn.classList.remove('hidden');
  likedItemsBtn.classList.remove('hidden');
  backBtn.classList.add('hidden');

  cleanUp();

  if (items.length > 0) {
    items
      .sort((a, b) => a.id - b.id)
      .forEach(({ id, value, likedState }) => {
        displayItem(id, value, likedState === 'unliked' ? 'like' : 'liked');
      });
  }

  itemsPage();
};

const displayEditingAlert = () => {
  alertMessage.classList.remove('hidden');
  alertMessage.textContent = `Error! You can't 
tweet about that`;
  setTimeout(() => {
    alertMessage.classList.add('hidden');
    alertMessage.textContent = '';
  }, twoSeconds);
};

const displayLikedAlert = () => {
  alertMessage.classList.remove('hidden');
  setTimeout(() => {
    alertMessage.classList.add('hidden');
    alertMessage.textContent = '';
  }, twoSeconds);
};

// ------------------------------------- Display Item -------------------------------------- //

const displayItem = (id, value, likeState) => {
  const newLi = document.createElement('li');
  const newP = document.createElement('p');
  const newDiv = document.createElement('div');
  const newRemoveBtn = document.createElement('button');
  const newLikeBtn = document.createElement('button');

  list.appendChild(newLi);

  newLi.appendChild(newP);
  newLi.appendChild(newDiv);

  newDiv.appendChild(newRemoveBtn);
  newDiv.appendChild(newLikeBtn);

  newLi.setAttribute('data-id', id);
  newLikeBtn.setAttribute('data-liked', 'unliked');
  newP.classList.add('tweetContent');

  newRemoveBtn.classList.add('remove-btn');
  newLikeBtn.classList.add('like-btn');

  newP.textContent = value;
  newRemoveBtn.textContent = 'remove';
  newLikeBtn.textContent = likeState || 'like';
  if (likeState === 'liked') {
    newLikeBtn.style.background = 'pink';
  }

  newP.addEventListener('click', (e) => {
    const id = e.target.parentElement.dataset.id;

    modifyItemInput.value = e.target.textContent;

    history.pushState({ id }, null, `#/edit/${id}`);

    editItemPage();
  });

  newRemoveBtn.addEventListener('click', () => {
    const items = JSON.parse(localStorage.getItem('items'));
    const targetItem = items.filter((item) => item.id === id)[0];

    const newItems = items.filter((item) => item.id !== targetItem.id);

    localStorage.setItem('items', JSON.stringify(newItems));
    newLi.remove();
  });

  newLikeBtn.addEventListener('click', (e) => {
    const id = e.target.parentElement.parentElement.dataset.id;

    const items = JSON.parse(localStorage.getItem('items'));
    const targetItem = items.filter((item) => item.id === id)[0];

    if (e.target.textContent === 'like') {
      e.target.dataset.liked = 'liked';
      targetItem.likedState = 'liked';
      e.target.textContent = 'liked';
      e.target.style.background = 'pink';
      if (backBtn.classList.contains('hidden')) {
        likedItemsBtn.classList.remove('hidden');
      }
      alertMessage.textContent = `Hooray! You liked tweet with id ${id}!`;
      displayLikedAlert();
    } else {
      e.target.dataset.liked = 'unliked';
      targetItem.likedState = 'unliked';
      e.target.textContent = 'like';
      e.target.style.background = '#EFEFEF';
      if (!backBtn.classList.contains('hidden')) {
        e.target.parentElement.parentElement.remove();
      }
      alertMessage.textContent = `Sorry you no longer like tweet with id ${id}`;
      displayLikedAlert();
    }

    if (items.every((item) => item.likedState === 'unliked')) {
      likedItemsBtn.classList.add('hidden');
    }

    localStorage.setItem('items', JSON.stringify(items));
  });
};

// ------------------------------------ Page Components ------------------------------------ //

const itemsPage = () => {
  if (!localStorage.getItem('items')) {
    localStorage.setItem('items', JSON.stringify([]));
  }
  backBtn.classList = 'backBtn hidden';
  if (!addTweetBtn.classList.contains('hidden')) {
    likedItemsBtn.style.marginLeft = '7rem';
  } else {
    likedItemsBtn.style.marginLeft = '0';
  }

  const items = JSON.parse(localStorage.getItem('items'));

  cleanUp();

  if (items.length > 0) {
    items
      .sort((a, b) => a.id - b.id)
      .forEach(({ id, value, likedState }) => {
        displayItem(id, value, likedState === 'unliked' ? 'like' : 'liked');
      });
  }

  if (items.some((item) => item.likedState === 'liked')) {
    likedItemsBtn.classList.remove('hidden');
  } else {
    likedItemsBtn.classList.add('hidden');
  }
};

const likedItemsPage = () => {
  const items = JSON.parse(localStorage.getItem('items'));
  const likedItems = items.filter((el) => el.likedState === 'liked');

  cleanUp();

  if (likedItems.length > 0) {
    likedItems
      .sort((a, b) => a.id - b.id)
      .forEach(({ id, value, likedState }) => {
        displayItem(id, value, likedState === 'unliked' ? 'like' : 'liked');
      });
  }

  addTweetBtn.classList.add('hidden');
  likedItemsBtn.classList.add('hidden');

  backBtn.classList.remove('hidden');
  backBtn.addEventListener('click', () => {
    history.pushState(null, '', '/index.html');
    homePage();
  });
};

const addItemPage = () => {
  tweetItems.classList.add('hidden');
  modifyItem.removeAttribute('class');
  modifyItemHeader.textContent = 'Add Tweet';
  modifyItemInput.value = '';
};

const editItemPage = () => {
  tweetItems.classList.add('hidden');
  modifyItem.removeAttribute('class');
  modifyItemHeader.textContent = 'Edit Tweet';
};

// ----------------------------------------- Logic ----------------------------------------- //
window.addEventListener('popstate', () => {
  console.log('popstate');

  if (!location.hash) {
    history.replaceState(null, '', '/index.html');
    homePage();
  } else {
    if (location.hash === '#/add') {
      addItemPage();
    }
    if (location.hash === '#/liked') {
      homePage();
      likedItemsPage();
    }
    if (location.hash.includes('#/edit/')) {
      const id = location.hash.match(/[0-9]{1,}/)[0];
      const items = JSON.parse(localStorage.getItem('items'));
      const targetItem = items.filter((item) => item.id === id)[0];

      modifyItemInput.value = targetItem.value;

      history.replaceState({ id }, null, `#/edit/${id}`);
      editItemPage();
    }
  }
});

document.addEventListener('DOMContentLoaded', () => {
  itemsPage();
});

addTweetBtn.addEventListener('click', () => {
  history.pushState(null, '', '#/add');
  addItemPage();
});

likedItemsBtn.addEventListener('click', () => {
  history.pushState(null, '', '#/liked');
  likedItemsPage();
});

saveModifiedItem.addEventListener('click', () => {
  if (history.state) {
    if (validateItem(modifyItemInput.value)) {
      const items = JSON.parse(localStorage.getItem('items'));
      const targetItem = items.filter(
        (item) => item.id === history.state.id
      )[0];

      targetItem.value = modifyItemInput.value;
      localStorage.setItem('items', JSON.stringify(items));

      history.pushState(null, '', '/index.html');
      homePage();
    } else {
      displayEditingAlert();
    }
  } else {
    if (validateItem(modifyItemInput.value)) {
      displayItem(new Date().getTime().toString(), modifyItemInput.value);

      const ul = document.querySelector('ul');
      const likeBtn = document.querySelector('.like-btn');

      const items = JSON.parse(localStorage.getItem('items'));

      const newItem = {
        id: ul.lastChild.dataset.id,
        value: modifyItemInput.value,
        likedState: likeBtn.dataset.liked
      };

      items.push(newItem);
      localStorage.setItem('items', JSON.stringify(items));

      history.pushState(null, '', '/index.html');
      homePage();
    } else {
      displayEditingAlert();
    }
  }
});

cancelModification.addEventListener('click', () => {
  history.replaceState(null, '', '/index.html');
  homePage();
});
