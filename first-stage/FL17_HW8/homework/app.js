const appRoot = document.getElementById('app-root');

const main = document.createElement('main');

const titleHeader = document.createElement('header');
const h1 = document.createElement('h1');

const filterSection = document.createElement('section');
const radioDiv = document.createElement('div');
const radioP = document.createElement('p');
const radioForm = document.createElement('form');
const filterByRegionFormControl = document.createElement('div');
const filterByRegionBtnLabel = document.createElement('label');
const filterByRegionBtn = document.createElement('input');
const filterByLanguageFormControl = document.createElement('div');
const filterByLanguageBtnLabel = document.createElement('label');
const filterByLanguageBtn = document.createElement('input');

const optionsDiv = document.createElement('div');
const optionsP = document.createElement('p');
const optionsForm = document.createElement('form');
const optionsSelect = document.createElement('select');
const staticOption = document.createElement('option');

const alertP = document.createElement('p');

const tableSection = document.createElement('section');
const table = document.createElement('table');
const staticTr = document.createElement('tr');
const countryNameTh = document.createElement('th');
const capitalTh = document.createElement('th');
const worldRegionTh = document.createElement('th');
const languagesTh = document.createElement('th');
const areaTh = document.createElement('th');
const flagTh = document.createElement('th');
const td = document.createElement('td');

appRoot.appendChild(main);

main.appendChild(titleHeader);
main.appendChild(filterSection);
main.appendChild(tableSection);

titleHeader.appendChild(h1);

filterSection.appendChild(radioDiv);
filterSection.appendChild(optionsDiv);

tableSection.appendChild(alertP);
tableSection.appendChild(table);

radioDiv.appendChild(radioP);
radioDiv.appendChild(radioForm);

optionsDiv.appendChild(optionsP);
optionsDiv.appendChild(optionsForm);

table.appendChild(staticTr);

radioForm.appendChild(filterByRegionFormControl);
radioForm.appendChild(filterByLanguageFormControl);

optionsForm.appendChild(optionsSelect);

staticTr.appendChild(countryNameTh);
staticTr.appendChild(capitalTh);
staticTr.appendChild(worldRegionTh);
staticTr.appendChild(languagesTh);
staticTr.appendChild(areaTh);
staticTr.appendChild(flagTh);

filterByRegionFormControl.appendChild(filterByRegionBtn);
filterByRegionFormControl.appendChild(filterByRegionBtnLabel);
filterByLanguageFormControl.appendChild(filterByLanguageBtn);
filterByLanguageFormControl.appendChild(filterByLanguageBtnLabel);

optionsSelect.appendChild(staticOption);

radioDiv.setAttribute('class', 'radioDiv');
optionsDiv.setAttribute('class', 'optionsDiv');

alertP.setAttribute('class', 'hidden');
table.setAttribute('class', 'hidden');

filterByRegionFormControl.setAttribute('class', 'filterByRegionFormControl');
filterByLanguageFormControl.setAttribute(
  'class',
  'filterByLanguageFormControl'
);

optionsSelect.setAttribute('class', 'select');

filterByRegionBtn.setAttribute('class', 'filterByRegionBtn');
filterByLanguageBtn.setAttribute('class', 'filterByLanguageBtn');
filterByRegionBtn.setAttribute('id', 'filterByRegionBtn');
filterByLanguageBtn.setAttribute('id', 'filterByLanguageBtn');

countryNameTh.setAttribute('class', 'countryTh');
areaTh.setAttribute('class', 'areaTh sort');

optionsSelect.disabled = true;

filterByRegionBtn.setAttribute('name', 'filter-btn');
filterByLanguageBtn.setAttribute('name', 'filter-btn');
filterByRegionBtn.setAttribute('type', 'radio');
filterByLanguageBtn.setAttribute('type', 'radio');
filterByRegionBtnLabel.setAttribute('for', 'filterByRegionBtn');
filterByLanguageBtnLabel.setAttribute('for', 'filterByLanguageBtn');

h1.textContent = 'Countries Search';
alertP.textContent = 'No items, please choose search query';

radioP.textContent = 'Please choose the type of search:';
optionsP.textContent = 'Please choose search query:';

countryNameTh.textContent = 'Country name';
capitalTh.textContent = 'Capital';
worldRegionTh.textContent = 'World region';
languagesTh.textContent = 'Languages';
areaTh.textContent = 'Area';
flagTh.textContent = 'Flag';

filterByRegionBtnLabel.textContent = 'By Region';
filterByLanguageBtnLabel.textContent = 'By Language';
staticOption.textContent = 'Select value';

const filterBtns = document.getElementsByName('filter-btn');
const select = document.querySelector('.select');

const cleanPreviousResults = (element) => {
  while (element.childNodes.length > 1) {
    element.removeChild(element.lastChild);
  }
};

const getOptions = (method, flag) => {
  if (flag === 'display') {
    cleanPreviousResults(optionsSelect);
    return method.forEach((filterOption) => {
      const option = document.createElement('option');
      optionsSelect.appendChild(option).textContent = filterOption;
    });
  }
  if (flag === 'get') {
    return method.map((filterOption) => filterOption);
  }

  throw new ReferenceError('flag is mandatory');
};

const sortList = (array) => {
  array.forEach((singleCountry) => {
    const tr = document.createElement('tr');
    table.appendChild(tr);

    const { name, capital, region, languages, area, flagURL } = singleCountry;
    const arrayOfProperties = [
      name,
      capital,
      region,
      Object.values(languages).join(', '),
      area,
      flagURL,
    ];

    arrayOfProperties.forEach((property) => {
      const td = document.createElement('td');
      tr.appendChild(td);
      td.textContent = property;
      if (td.textContent.startsWith('http')) {
        const img = document.createElement('img');
        img.src = td.textContent;
        td.appendChild(img);
        td.removeChild(td.childNodes[0]);
      }
    });
  });
};

const formList = () => {
  const regionsArray = getOptions(externalService.getRegionsList(), 'get');
  const languagesArray = getOptions(externalService.getLanguagesList(), 'get');

  let listOfCountries = [];

  const selectedValue = select.options[select.selectedIndex].text;
  if (regionsArray.find((el) => el === selectedValue)) {
    listOfCountries = externalService.getCountryListByRegion(selectedValue);
  }
  if (languagesArray.find((el) => el === selectedValue)) {
    listOfCountries = externalService.getCountryListByLanguage(selectedValue);
  }

  return listOfCountries;
};

const displayInitialList = () => {
  select.addEventListener('change', () => {
    alertP.classList.add('hidden');
    table.classList.remove('hidden');

    let listOfCountries = formList();

    cleanPreviousResults(table);

    sortList(listOfCountries.sort((a, b) => a.name.localeCompare(b.name)));
  });
};

for (let i = 0; i < filterBtns.length; i++) {
  filterBtns[i].addEventListener('change', () => {
    optionsSelect.disabled = false;
    alertP.classList.remove('hidden');
    table.classList.add('hidden');
  });
}

filterBtns[0].addEventListener('change', (e) => {
  getOptions(externalService.getRegionsList(), 'display');
  displayInitialList(e.target.classList);
});
filterBtns[1].addEventListener('change', (e) => {
  getOptions(externalService.getLanguagesList(), 'display');
  displayInitialList(e.target.classList);
});

countryNameTh.addEventListener('click', (e) => {
  cleanPreviousResults(table);

  let listOfCountries = formList();

  e.target.classList.toggle('sortZA');
  if (e.target.classList.contains('sortZA')) {
    sortList(listOfCountries.sort((a, b) => b.name.localeCompare(a.name)));
  } else {
    sortList(listOfCountries.sort((a, b) => a.name.localeCompare(b.name)));
  }
});

areaTh.addEventListener('click', (e) => {
  cleanPreviousResults(table);

  let listOfCountries = formList();

  e.target.classList.toggle('sort09');
  if (e.target.classList.contains('sort09')) {
    sortList(listOfCountries.sort((a, b) => a.area - b.area));
  } else {
    sortList(listOfCountries.sort((a, b) => b.area - a.area));
  }
});
