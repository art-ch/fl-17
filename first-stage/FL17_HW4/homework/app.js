// -------------------------------------- reverse number ----------------------------------- //

function reverseNumber(num) {
  const TEN = 10;
  let x = Math.abs(num);
  let y = 0;
  while (x > 0) {
    // eslint-disable-next-line no-extra-parens
    y = (x % TEN) + TEN * y;
    x = Math.floor(x / TEN);
  }
  return Math.sign(num) * y;
}

// ---------------------------------------- forEach ---------------------------------------- //

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    func(arr[i], i, arr);
  }
}

// -------------------------------------------- map ---------------------------------------- //

function map(arr, func) {
  const resultArray = new Array();
  forEach(arr, (el) => resultArray.push(func(el)));
  return resultArray;
}

// ------------------------------------------ filter --------------------------------------- //

function filter(arr, func) {
  const resultArray = new Array();
  forEach(arr, (el) => func(el) && resultArray.push(el));
  return resultArray;
}

// --------------------------------------- Apple Lovers ------------------------------------ //

function getAdultAppleLovers(data) {
  const legalAge = 18;
  const filteredData = filter(
    data,
    (person) => person.age >= legalAge && person.favoriteFruit === 'apple'
  );
  const names = map(filteredData, (el) => el.name);
  return names;
}

// ----------------------------------------- getKeys --------------------------------------- //

function getKeys(obj) {
  let resultArray = new Array();
  // eslint-disable-next-line guard-for-in
  for (keys in obj) {
    resultArray.push(keys);
  }
  return resultArray;
}

// ---------------------------------------- getValues -------------------------------------- //

function getValues(obj) {
  let resultArray = new Array();
  // eslint-disable-next-line guard-for-in
  for (const key in obj) {
    const value = obj[key];
    resultArray.push(value);
  }
  return resultArray;
}

// -------------------------------------- Format Date -------------------------------------- //

function showFormattedDate(dateObj) {
  const day = dateObj.getDate();
  const month = dateObj.toLocaleString('default', { month: 'short' });
  const year = dateObj.getFullYear();
  return `It is ${day} of ${month}, ${year}`;
}
