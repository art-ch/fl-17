// Your code goes here

// initial state
let maxNumber = 8;
let attempts = 3;
let prize = 100;

// i need these numbers for logic to work
const TWO = 2;
const THREE = 3;
const FOUR = 4;
const EIGHT = 8;

// get random number
const getRandomNumber = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

// get user number
const getUserNumber = (maxNumber, attempts, prize, total = 0) =>
  `Choose a roulette pocket number from 0 to ${maxNumber}
Attempts left: ${attempts}
Total prize: ${total}$
Possible prize on current attempt: ${prize}$`;

// game logic
const gameHandler = (total = 0) => {
  const randomNumber = getRandomNumber(0, maxNumber);
  let userNumber = prompt(getUserNumber(maxNumber, attempts, prize, total));
  if (userNumber === '') {
    location.reload();
  } else if (userNumber && userNumber !== '') {
    userNumber = Number(userNumber);
    if (randomNumber === userNumber) {
      total += prize;
      if (
        confirm(
          `Congratulation, you won! Your prize is: ${total}$. Do you want to continue?`
        )
      ) {
        maxNumber += FOUR;
        prize *= TWO;
        gameHandler(total);
      }
    } else {
      while (randomNumber !== userNumber && attempts > 1) {
        attempts -= 1;
        prize /= TWO;
        userNumber = Number(
          prompt(getUserNumber(maxNumber, attempts, prize, total))
        );
        if (randomNumber === userNumber) {
          total += prize;
          if (
            confirm(
              `Congratulation, you won! Your prize is: ${total}$. Do you want to continue?`
            )
          ) {
            maxNumber += FOUR;
            if (attempts === TWO) {
              prize *= FOUR;
            }
            if (attempts === 1) {
              prize *= EIGHT;
            }
            attempts = THREE;
            gameHandler(total);
          }
        }
        if (attempts === 1 && randomNumber !== userNumber) {
          alert(`Thank you for your participation. Your prize is: ${total}$`);
          confirm('Do you want to play again?') && location.reload();
        }
        if (!userNumber) {
          alert(`Thank you for your participation. Your prize is: ${total}$`);
          confirm('Do you want to play again?') && location.reload();
          break;
        }
      }
    }
  } else {
    alert(`Thank you for your participation. Your prize is: ${total}$`);
    confirm('Do you want to play again?') && location.reload();
  }
};

// game itself
const intro = confirm('Do you want to play a game?');
if (intro) {
  gameHandler();
} else {
  alert('You did not become a billionaire, but can.');
}
