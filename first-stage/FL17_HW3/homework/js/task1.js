// Your code goes here

// Regular Constants
const initialAmount = Number(window.prompt('Please enter initial amount'));
const numberOfYears = Number(window.prompt('Please enter number of years'));
const annualPercentageYield = Number(
  window.prompt('Please enter your interest')
);
// No-magic-numbers related explicit constants
const minAmount = 1000;
const minNumberOfYears = 1;
const maxPercentage = 100;

// I need this particular values without warning
const FIXED_DECIMAL_POINTS = 2;
const HUNDRED = 100;

const getResult = () => {
  // Invalid Conditions
  const noValues = !initialAmount || !numberOfYears || !annualPercentageYield;
  const nanValues =
    isNaN(initialAmount) ||
    isNaN(numberOfYears) ||
    isNaN(annualPercentageYield);
  const illegalValues =
    initialAmount < minAmount ||
    numberOfYears < minNumberOfYears ||
    numberOfYears % 1 !== 0 ||
    annualPercentageYield > maxPercentage;

  // Validation
  if (noValues || nanValues || illegalValues) {
    return alert('Invalid input data');
  }

  // get Math.pow base parameter
  const getBase = 1 + annualPercentageYield / HUNDRED;

  // get alert values
  const totalAmount = (
    initialAmount * Math.pow(getBase, numberOfYears)
  ).toFixed(FIXED_DECIMAL_POINTS);

  const totalProfit = (totalAmount - initialAmount).toFixed(
    FIXED_DECIMAL_POINTS
  );

  alert(
    `Initial amount: ${initialAmount}
Number of years: ${numberOfYears}
Percentage of year: ${annualPercentageYield}

Total profit: ${totalProfit}
Total amount: ${totalAmount}`
  );
};
getResult();
